#!/bin/bash
echo "Installing mariadb or mysql from shell script"

echo " this is script works for RHEL7/8 and CentOS 7/8"

sudo yum install -y mariadb mariadb-server

sudo yum list installed | grep mariadb

sudo systemctl start mariadb

sudo systemctl enable mariadb

sudo systemctl status mariadb

sudo firewall-cmd --permanent --add-service mysql

sudo firewall-cmd --reload

echo "mysql-db has been installed and started the related services"

