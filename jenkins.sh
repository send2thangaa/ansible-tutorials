#!/bin/bash

echo "______________________________ Welcome my automation world !! _____________________________"

sleep 2

echo "_____________________________ Java installation begins _________________________"

sudo yum install -y java 

echo "_____________________________ Adding Jenkins Redhat Packages to repos _______________________"

sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sleep 2

echo "_____________________________ The actual jenkins installatioin process kicksin _________________________"

sudo yum install jenkins -y

sudo systemctl start jenkins

sudo systemctl enable jenkins

sudo systemctl status jenkins

echo " _____________________________________ Jenkins started running, you're good to use of it ______________________"

sudo chown -R root:root /var/lib/jenkins
sudo chown -R root:root /var/cache/jenkins
sudo chown -R root:root /var/log/jenkins
