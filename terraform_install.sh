#################################################################
# Instllation of Terraform on Amazon EC2 instances              #
# Author: Thangadurai, Murugan                                  #
# Date: 6/2/2021                                                #
# Version: 0.01                                                 #
#################################################################

echo "______________________________  SCRIPT BEGINS  ____________________"



#!/bin/bash

echo "downloading terraform binaries from the web"

echo "extra packages installing on AWS EC2 instance"

echo "_______________________________ IN PROGRESS ______________________________"

sudo yum install -y bind-utils

wget https://releases.hashicorp.com/terraform/0.15.4/terraform_0.15.4_linux_amd64.zip -O terra.zip

unzip terra.zip; sudo rm -rf terra.zip

sudo mv terraform /usr/local/bin

sleep 2

echo "_____________________________ Checking Terraform Version _______________________"

terraform version 

echo "SCRIPT ENDS"

