echo "Installing Docker on the system" 
sudo groupadd docker
sudo usermod -a -G docker ec2-user
sudo yum install docker -y 
sudo systemctl enable docker
sudo systemctl start docker
sudo systemctl status docker
echo "Docker has been installed"
echo " checking the sleep version of it"
docker version

## Note: Though after configured above steps, still docker commands are not working, use below command
sudo chmod 666 /var/run/docker.sock

