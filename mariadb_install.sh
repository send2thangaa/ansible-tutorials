#!/bin/bash

echo "_____________ Script begins _______________"

echo "____________ Installing mariadb or mysql _______________"

echo " this is script works for RHEL7/8 and CentOS 7/8"

sudo yum install -y mariadb mariadb-server

sudo yum list installed | grep mariadb

sudo systemctl start mariadb

sudo systemctl enable mariadb

sudo systemctl status mariadb

sudo firewall-cmd --permanent --add-service mysql

sudo firewall-cmd --reload

sleep 5

echo "________________ DB installation completed __________________"
